\chapter{Параллельная реализация метода переменных направлений}
\section{Декомпозиция расчётной области по процессам}
При помощи схемы переменных направлений совершён переход от системы из
$\mathcal{O}(N_1 N_2)$ уравнений к $\mathcal{O}(N_2)$ независимых систем из
$\mathcal{O}(N_1)$ уравнений по направлению $x$ и $\mathcal{O}(N_1)$ независимых систем из
$\mathcal{O}(N_2)$ уравнений по направлению $z$.
Из этого естественным образом следует разбиение расчётной области между процессами
для параллельного алгоритма построения решения. При вычислении в многопроцессорной
среде с $p$ процессами сетка может быть разделена на $p$ полос
параллельно неявному направлению, как показано на рисунке \ref{fig:decomposition}.

\begin{figure}[ht]
\centering
\begin{tikzpicture}[scale=1.2,every node/.style={minimum size=1cm},on grid]
    \begin{scope}[
            yshift=-40,every node/.append style={
            yslant=0.5,xslant=-1.3},yslant=0.5,xslant=-1.3
        ]
        \coordinate (taxis) at (0,4);
    \end{scope}

    \begin{scope}[
            yshift=-160,every node/.append style={
            yslant=0.5,xslant=-1.3},yslant=0.5,xslant=-1.3
        ]
        \draw[black,very thick] (0.0,0) rectangle (0.8,4);
        \draw[black,very thick] (0.8,0) rectangle (1.6,4);
        \draw[black,very thick] (1.6,0) rectangle (2.4,4);
        \draw[black,very thick] (3.2,0) rectangle (4.0,4);

        \node at (0.4,0.4) [scale=1.5] {$0$};
        \node at (1.2,0.4) [scale=1.5] {$1$};
        \node at (2.0,0.4) [scale=1.5] {$2$};
        \node at (2.8,0.4) [scale=1.5] {$\cdots$};
        \node at (3.6,0.4) [scale=1.5] {$p$};

        \draw[thick,->] (0,4) -- (0,-1) node[anchor=north west] {}; %x axis
        \draw[thick,->] (0,4) -- (5,4) node[anchor=south east] {}; %z axis
        \draw[thick,->] (0,4) -- (taxis) node {}; %t axis
    \end{scope}

    \node at (1.0,-6.2) {$\mathsf{x}$};
    \node at (-0.2,-1.4) {$\mathsf{z}$};
    \node at (-5,0) {$\mathsf{t}$};

    \begin{scope}[
            yshift=-100,every node/.append style={
            yslant=0.5,xslant=-1.3},yslant=0.5,xslant=-1.3
        ]
        % opacity to prevent graphical interference
        \fill[white,fill opacity=0.8] (0,1.6) rectangle (4,4.0);
        \fill[white,fill opacity=0.8] (0,0.0) rectangle (4,0.8);
        \draw[black,very thick] (0,0.0) rectangle (4,0.8);
        \draw[black,very thick] (0,1.6) rectangle (4,2.4);
        \draw[black,very thick] (0,2.4) rectangle (4,3.2);
        \draw[black,very thick] (0,3.2) rectangle (4,4.0);

        \node at (3.6,3.6) [scale=1.5] {$0$};
        \node at (3.6,2.8) [scale=1.5] {$1$};
        \node at (3.6,2.0) [scale=1.5] {$2$};
        \node at (3.6,1.2) [scale=1.5] {$\vdots$};
        \node at (3.6,0.4) [scale=1.5] {$p$};
    \end{scope}
\end{tikzpicture}
\caption{Декомпозиция расчётной области и распределение данных по процессам}
\label{fig:decomposition}
\end{figure}

На всяком временном слое каждый процесс владеет некоторой непрерывной полосой, являющейся частью
всей расчётной области. Производится разбиение, при котором ширина любой
полосы равна $\left\lfloor \frac{N_1}{p} \right\rfloor$ либо $\left\lfloor \frac{N_1}{p} + 1 \right\rfloor$
по направлению $x$, $\left\lfloor \frac{N_2}{p} \right\rfloor$ либо
$\left\lfloor \frac{N_2}{p} + 1 \right\rfloor$ по направлению $z$.

При переходе от слоя к слою совершается обмен данными между процессами, причём каждый
процесс должен передать соответствующую часть своей области каждому из процессов,
в том числе себе. Имеет место модель обмена сообщениями, называемая ``All-to-All
Personalized Communication''. Она по своей сути подразумевает передачу между процессами
порядка $\mathcal{O}(N_1N_2)$ данных. Библиотека распределённых вычислений MPI предоставляет
API для осуществления коммуникаций по данной модели \cite{OpenMPI}:

\begin{itemize}
    \item \texttt{MPI\_Alltoall} --- обмен одинаковым числом объектов одного типа,
    \item \texttt{MPI\_Alltoallv} --- обмен разным числом объектов одного типа,
    \item \texttt{MPI\_Alltoallw} --- обмен разным числом объектов разных типов.
\end{itemize}

В силу неодинаковости ширин полос, на которые разделено пространство вычислений, возникает
потребность в использовании разных типов и передаче различного числа объектов.
Поэтому при реализации выбран метод \texttt{MPI\_Alltoallw}.

Использование различных типов для передачи сообщений открывает возможности для
оптимизации укладки данных в памяти. Предпочтительным
является построчное хранение вычислительной сетки для первого шага метода переменных
направлений и поколоночное --- для второго шага, поскольку вычисления производятся
вдоль строк и столбцов соответственно. Передавая данные поколоночно и принимая данные
построчно, можно объединить транспонирование области и её передачу, тем самым
избавившись от явного транспонирования сетки или неоптимального её обхода.

Помимо функциональных групповых пересылок данных, коих производится по две на каждый шаг 
$j \rightarrow j+1$ алгоритма, необходим также сбор данных в единую сетку на одном
из процессов для последующего сохранения результатов во внешней памяти. Для этого
может быть использован метод \texttt{MPI\_Gatherv}, осуществляющий сбор различных
фрагментов вычислительной области.

Псевдокод параллельного алгоритма решения уравнения приведён в листинге \ref{lst:parallel}

\begin{lstlisting}[escapeinside={(*}{*)},
    caption={Псевдокод параллельного алгоритма для процесса k},
    label={lst:parallel},
]
do (*$j = \overline{1, T}$*)
    do (*$i_2 = \overline{S_{2, k}, S_{2, k + 1}}$*)
        do
            do (*$i_1 = \overline{1, N_1 - 1}$ \textcolor{gray}{// Прямой ход прогонки}*)
                (*$\alpha_{i_1+1} = f_1(\alpha_{i_1},
                y^{j}_{i_1, i_2-1},y^{j}_{i_1, i_2}, y^{j}_{i_1, i_2+1} )$*)
                (*$\beta_{i_1+1} = f_2(\alpha_{i_1}, \beta_{i_1},
                y^{j}_{i_1, i_2-1},y^{j}_{i_1, i_2}, y^{j}_{i_1, i_2+1} )$*)
            enddo
            do (*$i_1 = \overline{N_1 - 1, 1}$ \textcolor{gray}{// Обратный ход прогонки}*)
                (*$\overset{s+1}{y^{j+\frac{1}{2}}_{i_1, i_2}} =
                \alpha_{i_1+1} \overset{s+1}{y^{j+\frac{1}{2}}_{i_1+1, i_2}} + \beta_{i_1+1}$*)
                (*$\delta = \max(\delta,
                \frac{\left|\overset{s+1}{y^{j+1}_{i_1, i_2}} -
                \overset{s}{y^{j+1}_{i_1, i_2}}\right|}{\overset{s}{y^{j+1}_{i_1, i_2}}})$*)
            enddo
        while (*$\delta > \varepsilon$*)
    enddo
    alltoall((*$y^{j+\frac{1}{2}}$*))
    do (*$i_1 = \overline{S_{1, k}, S_{1, k + 1}}$*)
        do
            do (*$i_2 = \overline{1, N_2 - 1}$ \textcolor{gray}{// Прямой ход прогонки}*)
                (*$\alpha_{i_2+1} = f_3(\alpha_{i_2},
                y^{j+\frac{1}{2}}_{i_1-1, i_2},
                y^{j+\frac{1}{2}}_{i_1, i_2}, y^{j+\frac{1}{2}}_{i_1+1, i_2} )$*)
                (*$\beta_{i_2+1} = f_4(\alpha_{i_2}, \beta_{i_2},
                y^{j+\frac{1}{2}}_{i_1-1, i_2},
                y^{j+\frac{1}{2}}_{i_1, i_2}, y^{j+\frac{1}{2}}_{i_1+1, i_2} )$*)
            enddo
            do (*$i_2 = \overline{N_2 - 1, 1}$ \textcolor{gray}{// Обратный ход прогонки}*)
                (*$\overset{s+1}{y^{j+1}_{i_1, i_2}} =
                \alpha_{i_2+1} \overset{s+1}{y^{j+1}_{i_1, i_2+1}} + \beta_{i_2+1}$*)
                (*$\delta = \max(\delta,
                \frac{\left|\overset{s+1}{y^{j+1}_{i_1, i_2}} -
                \overset{s}{y^{j+1}_{i_1, i_2}}\right|}{\overset{s}{y^{j+1}_{i_1, i_2}}})$*)
            enddo
        while (*$\delta > \varepsilon$*)
    enddo
    alltoall((*$y^{j+1}$*))
enddo
\end{lstlisting}

Массивы $S_{1,k}$ и $S_{2,k}$ содержат индексы начала полосы для k-го процесса
по направлению $x$ и $z$ соответственно. Они могут быть вычислены следующим образом:

\begin{gather*}
    S_{1,k} = k \cdot \left\lfloor \frac{N_1}{p} \right\rfloor +
    \min(k, N_1 - p \cdot \left\lfloor \frac{N_1}{p} \right\rfloor)\\
    S_{2,k} = k \cdot \left\lfloor \frac{N_2}{p} \right\rfloor +
    \min(k, N_2 - p \cdot \left\lfloor \frac{N_2}{p} \right\rfloor)
\end{gather*}

Следует отметить, что при вычислении коэффициентов для нахождения решения
системы уравнений методом прогонки по одному направлению
необходимы значения сеточной функции с предыдущего слоя по другому направлению.
Например, $y^{j+\frac{1}{2}}_{i_1, i_2}$ зависит от $y^{j}_{i_1, i_2-1}$,
$y^{j}_{i_1, i_2}$ и $y^{j}_{i_1, i_2+1}$. Явная и неявная зависимость
вычисляемого значения сеточной функции наглядно отражена на рисунке \ref{fig:deps}.

\begin{figure}[ht]
\centering
\begin{tikzpicture}[scale=1.2,every node/.style={minimum size=1cm},on grid]
    \begin{scope}[
            yshift=-40,every node/.append style={
            yslant=0.5,xslant=-1.3},yslant=0.5,xslant=-1.3
        ]
        \coordinate (taxis) at (0,4);
    \end{scope}

    \begin{scope}[
            yshift=-160,every node/.append style={
            yslant=0.5,xslant=-1.3},yslant=0.5,xslant=-1.3
        ]
        \draw[step=8mm, thin, gray] (0,0) grid (4,4); % defining grids
        \draw[black,very thick] (0,0) rectangle (4,4); % marking border

        \coordinate (y00) at (1.2,2.0);
        \coordinate (y01) at (2.0,2.0);
        \coordinate (y02) at (2.8,2.0);

        \node at (y00) [fill=black,circle,scale=0.3] {};
        \node at (y01) [fill=black,circle,scale=0.3] {};
        \node at (y02) [fill=black,circle,scale=0.3] {}; 

        \draw[thick,->] (0,4) -- (0,-1) node[anchor=north west] {}; % x axis
        \draw[thick,->] (0,4) -- (5,4) node[anchor=south east] {}; % z axis
        \draw[thick,->] (0,4) -- (taxis) node {}; % t axis
    \end{scope}

    % coordinates
    \node at (1.0,-6.2) {$\mathsf{x}$};
    \node at (-0.2,-1.4) {$\mathsf{z}$};
    \node at (-5,0) {$\mathsf{t}$};

    \begin{scope}[
            yshift=-100,every node/.append style={
            yslant=0.5,xslant=-1.3},yslant=0.5,xslant=-1.3
        ]
        % opacity to prevent graphical interference
        \fill[white,fill opacity=0.8] (0,0) rectangle (4,4);
        \draw[step=8mm, thin, gray] (0,0) grid (4,4); % defining grids
        \draw[black,very thick] (0,0) rectangle (4,4); % marking borders      

        \coordinate (y10) at (2.0,1.2);
        \coordinate (y11) at (2.0,2.0);
        \coordinate (y12) at (2.0,2.8);

        \node at (y10) [fill=black,circle,scale=0.3] {};
        \node at (y11) [fill=black,circle,scale=0.3] {};
        \node at (y12) [fill=black,circle,scale=0.3] {};

    \end{scope}

    \draw[ultra thick,-] (y01) -- (y11) node {};
    \draw[ultra thick,-] (y00) -- (y02) node {};
    \draw[ultra thick,-] (y10) -- (y12) node {}; 
\end{tikzpicture}
\caption{Схема зависимостей сеточной функции}
\label{fig:deps}
\end{figure}

Данного рода зависимости должны быть учтены при реализации коммуникаций между процессами.
На рисунке \ref{fig:deps} изображён переход от основного временного слоя к промежуточному.
На промежуточном слое процессом $i$ решается $s_i \approx \frac{N_2}{p}$
одномерных задач по направлению оси $x$. Для этого ему необходимы значения сеточной
функции с предыдущего временного слоя в нижележащей полосе шириной $s_i + 2$.

\section{Программная реализация библиотеки, её интерфейс и конфигурация}

Для реализации библиотеки выбран язык C++14. В качестве имплементации стандарта MPI
выбран пакет OpenMPI \cite{OpenMPI}. Он также предоставляет C++-обёртку для C-интерфейса библиотеки MPI.
Проект конфигурируется и собирается при помощи системы сборки CMake \cite{CMake}. Исходный код библиотеки
находится в свободном доступе по адресу: \url{https://gitlab.com/jisuto/dissertation}.

Библиотека предоставляет возможность параллельного решения квазилинейных неоднородных двумерных
параболических уравнений с граничными условиями первого рода в вычислительной среде с
распределённой памятью. Основная логика решения уравнения инкапсулирована в классе \texttt{TSolver}.
Он, в свою очередь, конфигурируется следующими компонентами:

\begin{itemize}
    \item \texttt{TGridParams} --- параметры дискретизации времени и пространства.
        \texttt{h1}, \texttt{h2} и \texttt{tau} задают шаги $h_1$, $h_2$ и $\tau$;
        \texttt{N1} и \texttt{N2} задают размеры области $N_1$ и $N_2$;
        \texttt{T} определяет число временных итераций. Эти величины определены в разделе \ref{section:steps}.
    \item \texttt{TProperties} --- параметры уравнения $c$, $\rho(T)$, $\lambda(T)$ и $Q(x, z, t)$.
    \item \texttt{TBoundaries} --- граничные условия $\mu_1$, $\mu_2$, $\mu_3$ и $\mu_4$;
        начальное условие $u_0$.
    \item \texttt{IMaterial} --- абстрактный класс, инкапсулирующий свойства среды.
        В рассматриваемой практической задаче \cite{poroshok} область имеет неоднородную структуру,
        состоящую из ячеек порошкообразных реагентов Ti + C, продукта реакции карбида титана TiC,
        нихрома в качестве связующего инертного материала и подложки. Различия между материалами выражены в их
        механических и термодинамических характеристиках и определяются параметрами $c$, $\rho$, $\lambda$.
        Задаваемый классом \texttt{IMaterial} интерфейс требует от конкретных реализаций наличия
        метода, возвращающего свойства \texttt{TProperties} по координатам $x$ и $z$.
    \item \texttt{TUniformMaterial} --- пример реализации интерфейса \texttt{IMaterial},
        соответствующий гомогенной среде с одинаковыми во всех точках свойствами.
    \item \texttt{TGridMaterial} --- ещё один наследник класса \texttt{IMaterial},
        позволяющий определять гетерогенную среду на основании сетки с некоторым шагом.
        При конструировании он принимает структуру \texttt{TGrid}, определяющую размеры
        и шаг сетки, аналогичные передаваемым в класс \texttt{TSolver} пространственным параметрам
        в стуктуре \texttt{TGridParams}, однако в общем случае им не равные. Напротив,
        естественна ситуация, когда детализация укладки материала ниже детализации моделирования.
        Помимо параметров сетки \texttt{TGrid} содержит саму сетку как массив
        свойств \texttt{TProperties}. Через класс \texttt{TGridMaterial} осуществляется
        доступ по реальным координатам $x$ и $z$ к свойствам среды.
    \item \texttt{TDumpPrams} --- конфигурация сохранения вычисляемого решения в файл.
        Определяет частоту, с которой инициируется сбор данных со всех процессов и запись
        их в файл, а также имя этого файла. В случае, если выгрузка данных не требуется, частота
        выгрузки устанавливается равной нулю. Это значение является значением по умолчанию,
        а сам конфигуратор типа \texttt{TDumpParams} не является обязательным при создании
        экземпляра класса \texttt{TSolver}.
\end{itemize}

За решение заданного уравнения отвечает метод \texttt{TSolver::Run()}.

Проект также содержит примеры использования описанных выше сущностей и интерфейсов.
О них пойдёт речь в разделе \ref{section:analytical} и главе \ref{section:plasma_jet}.

\section{Вычислительные эксперименты}\label{section:analytical}

Для тестирования разработанной библиотеки выбрано квазилинейное
неоднородное двумерное параболическое уравнение вида:
\begin{gather*}
    \rho(T) c \frac{\partial T}{\partial t} =
    \frac{\partial}{\partial x} \left( \lambda(T) \frac{\partial T}{\partial x} \right) +
    \frac{\partial}{\partial z} \left( \lambda(T) \frac{\partial T}{\partial z} \right) +
    Q(x, z, t),\\
    (x, z) \in G = [0<x<l_1] \times [0<z<l_2], t \in [0<t \le T],
\end{gather*}
с начальным условием
\begin{gather*}
    u(x, z, 0) = u_0(x, z), (x, z) \in G,
\end{gather*}
и краевым условием
\begin{gather*}
    \left. u(x, z, t) \right\rvert_{\Gamma} = \mu(x, z, t).
\end{gather*}

Для проведения вычислительного экперимента, тестирования и отладки использовалось
уравнение c параметрами:
\begin{gather*}
    \rho(T) = c = \lambda(T) = 1,\\
    Q(x, z, t) = -e^{x+z+t},
\end{gather*}
и имеющее решение:
\begin{gather*}
    u(x, z, t) = e^{x+z+t}
\end{gather*}

Все экcперименты проводились в области $[0 < x < 0.1] \times [0 < z <0.1] \times [0 < t <1]$.

В таблице \ref{table:space} представлены значения абсолютной погрешности вычислений
на последнем временном слое при $T=100$.

\begin{table}[ht]
\centering
\begin{tabular}{|l|l|}
    \hline
    Размер сетки & Погрешность  \\ \hline
    $10$ & $3.47407 \cdot 10^{-5}$ \\
    $100$ & $3.96189 \cdot 10^{-5}$ \\
    $1000$ & $4.0523 \cdot 10^{-5}$ \\ \hline
\end{tabular}
\caption{Погрешность при различных размерах сетки по пространственным переменным}
\label{table:space}
\end{table}

В таблице \ref{table:time} приведены значения погрешности при $N_1 = N_2 = 100$
и различных $T$.

\newpage

\begin{table}[ht]
\centering
\begin{tabular}{|l|l|}
    \hline
    T & Погрешность  \\ \hline
    $100$ & $3.96189 \cdot 10^{-5}$ \\
    $500$ & $1.87231 \cdot 10^{-6}$ \\
    $1000$ & $7.41514 \cdot 10^{-7}$ \\
    $5000$ & $1.1783 \cdot 10^{-7}$ \\
    $10000$ & $5.68482 \cdot 10^{-8}$ \\ \hline
\end{tabular}
\caption{Погрешность при различных размерах сетки по времени}
\label{table:time}
\end{table}

Уменьшение шага сетки по пространственным переменным
приводит к незначительному уменьшению точности. В то же время
уменьшение временного шага $\tau$ существенно увеличивает точность.
Это следует из погрешности разностной схемы, которая равна $\mathcal{O}(\tau + h^2)$.

