#pragma once

#include <cstddef>
#include <string>

struct TDumpParams {
    size_t Frequency = 0;
    std::string Filename;
};
