#pragma once

#include "material.h"

#include <vector>

struct TGrid {
    size_t N1;
    size_t N2;
    double h1;
    double h2;
    std::vector<TProperties> Cells;
};

class TGridMaterial : public IMaterial {
public:
    explicit TGridMaterial(TGrid grid);
    const TProperties& GetProperties(double x, double z) const final;

private:
    TGrid Grid_;
};
