#include "material.h"

TUniformMaterial::TUniformMaterial(TProperties params)
    : Properties_(std::move(params))
{
}

const TProperties& TUniformMaterial::GetProperties(double /*x*/, double /*z*/) const {
    return Properties_;
}
