#include "solver.h"

#include <cassert>
#include <fstream>
#include <mpi.h>

static constexpr double EPS = 1e-6;

TSolver::TSolver(std::unique_ptr<IMaterial> material, TBoundaries boundaries, TGridParams gridParams, TDumpParams dumpParams)
    : Material_(std::move(material))
    , Boundaries_(std::move(boundaries))
    , GridParams_(std::move(gridParams))
    , DumpParams_(std::move(dumpParams))
{
}

void TSolver::Run() const {
    MPI::Comm& world = MPI::COMM_WORLD;

    // Get the number of processes.
    const size_t worldSize = world.Get_size();

    // Get the rank of the process.
    const size_t rank = world.Get_rank();

    const auto N1 = GridParams_.N1;
    const auto N2 = GridParams_.N2;

    std::vector<int> stripes1(worldSize);
    std::vector<int> stripes2(worldSize);
    std::vector<int> paddedStripes1(worldSize);
    std::vector<int> paddedStripes2(worldSize);

    std::vector<int> hSendDispls(worldSize);
    std::vector<int> vSendDispls(worldSize);
    std::vector<int> hRecvDispls(worldSize);
    std::vector<int> vRecvDispls(worldSize);
    std::vector<MPI::Datatype> vSendTypes(worldSize);
    std::vector<MPI::Datatype> vRecvTypes(worldSize);

    if (rank == 0) {
        hSendDispls[0] = -1;
        vSendDispls[0] = -1;
    } else {
        hSendDispls[0] = N1 - 2;
        vSendDispls[0] = N2 - 2;
    }

    for (size_t i = 0; i < worldSize; ++i) {
        stripes1[i] = (N1 - 1) / worldSize + (i < (N1 - 1) % worldSize);
        stripes2[i] = (N2 - 1) / worldSize + (i < (N2 - 1) % worldSize);
        paddedStripes1[i] = stripes1[i] + (i > 0) + (i < worldSize - 1);
        paddedStripes2[i] = stripes2[i] + (i > 0) + (i < worldSize - 1);

        vSendTypes[i] = MPI::DOUBLE.Create_vector(1, paddedStripes2[i], 1).Create_resized(0, sizeof(double) * (N2 - 1));
        vRecvTypes[i] = MPI::DOUBLE.Create_vector(1, stripes2[i], 1).Create_resized(0, sizeof(double) * (N2 - 1));
        vSendTypes[i].Commit();
        vRecvTypes[i].Commit();

        if (i > 0) {
            hSendDispls[i] = hSendDispls[i - 1] + stripes1[i - 1];
            vSendDispls[i] = vSendDispls[i - 1] + stripes2[i - 1];
            hRecvDispls[i] = hRecvDispls[i - 1] + stripes1[i - 1];
            vRecvDispls[i] = vRecvDispls[i - 1] + stripes2[i - 1];
        }
    }

    // As first stripe has no padding from the left, point first displacement to the beginning of working tile.
    ++hSendDispls[0];
    ++vSendDispls[0];

    // Multiply all displacements by sizeof.
    for (size_t i = 0; i < worldSize; ++i) {
        hSendDispls[i] *= sizeof(double);
        vSendDispls[i] *= sizeof(double);
        hRecvDispls[i] *= sizeof(double);
        vRecvDispls[i] *= sizeof(double);
    }

    MPI::Datatype hSendType = MPI::DOUBLE.Create_vector(stripes2[rank], 1, (N1 - 1)).Create_resized(0, sizeof(double));
    MPI::Datatype hRecvType = MPI::DOUBLE.Create_vector(paddedStripes2[rank], 1, (N1 - 1)).Create_resized(0, sizeof(double));
    hSendType.Commit();
    hRecvType.Commit();
    const std::vector<MPI::Datatype> hSendTypes(worldSize, hSendType);
    const std::vector<MPI::Datatype> hRecvTypes(worldSize, hRecvType);

    const std::vector<int> vSendSizes(worldSize, stripes1[rank]);
    const std::vector<int> vRecvSizes(worldSize, paddedStripes1[rank]);

    std::vector<double> hLayer((N1 - 1) * paddedStripes2[rank]);
    std::vector<double> hSublayer((N1 - 1) * paddedStripes2[rank]);

    std::vector<double> vLayer((N2 - 1) * paddedStripes1[rank]);
    std::vector<double> vSublayer((N2 - 1) * paddedStripes1[rank]);

    // For gathering.
    MPI::Datatype row = MPI::DOUBLE.Create_vector(1, GridParams_.N1 - 1, 1);
    row.Commit();

    std::ofstream fout;
    if (DumpParams_.Frequency > 0 && rank == 0) {
        fout.open(DumpParams_.Filename);
    }

    InitZeroLayer(rank, worldSize, hLayer);

    for (size_t j = 0; j < GridParams_.T; ++j) {
        // Forward sweep.
        Step1(j, rank, worldSize, hLayer, hSublayer);

        world.Alltoallw(
            hSublayer.data(), paddedStripes1.data(), hSendDispls.data(), hSendTypes.data(),
            vSublayer.data(), vRecvSizes.data(), vRecvDispls.data(), vRecvTypes.data());

        // Backward sweep.
        Step2(j, rank, worldSize, vSublayer, vLayer);

        world.Alltoallw(
            vLayer.data(), vSendSizes.data(), vSendDispls.data(), vSendTypes.data(),
            hLayer.data(), stripes1.data(), hRecvDispls.data(), hRecvTypes.data());

        const void* const displacedSendBuf = reinterpret_cast<char*>(hLayer.data()) + hSendDispls[0];
        DumpLayer(displacedSendBuf, j, stripes2, row, fout);
    }
}

size_t TSolver::GetIdx(const size_t i, const size_t j) const {
    return GetIdx(i, j, GridParams_.N1 - 1);
}

size_t TSolver::GetIdx(const size_t i, const size_t j, const size_t offset) const {
    return i * offset + j;
}

double TSolver::Idx2Value(const size_t i, const double step) const {
    return (i + 1) * step;
}

double TSolver::lambda(const size_t i1, const size_t i2, const double T) const {
    const double x = Idx2Value(i1, GridParams_.h1);
    const double z = Idx2Value(i2, GridParams_.h2);

    return Material_->GetProperties(x, z).lambda(T);
}

double TSolver::rho(const size_t i1, const size_t i2, const double T) const {
    const double x = Idx2Value(i1, GridParams_.h1);
    const double z = Idx2Value(i2, GridParams_.h2);

    return Material_->GetProperties(x, z).rho(T);
}

double TSolver::c(const size_t i1, const size_t i2) const {
    const double x = Idx2Value(i1, GridParams_.h1);
    const double z = Idx2Value(i2, GridParams_.h2);

    return Material_->GetProperties(x, z).c;
}

double TSolver::muSublayer(const size_t i1, const size_t i2, const size_t j) const {
    assert(i1 == -1 || i1 == GridParams_.N1 - 1);

    const double z0 = Idx2Value(i2 - 1, GridParams_.h2);
    const double z1 = Idx2Value(i2, GridParams_.h2);
    const double z2 = Idx2Value(i2 + 1, GridParams_.h2);

    const double t0 = j * GridParams_.tau;
    const double t1 = (j + 0.5) * GridParams_.tau;
    const double t2 = (j + 1) * GridParams_.tau;

    const auto& mu = i1 == -1 ? Boundaries_.mu1 : Boundaries_.mu2;

    const double b1 = (lambda(i1, i2, mu(z1, t1)) + lambda(i1, i2 - 1, mu(z0, t1))) / 2;
    const double b2 = (lambda(i1, i2 + 1, mu(z2, t1)) + lambda(i1, i2, mu(z1, t1))) / 2;

    // mu^{j+1} - mu^{j}
    const double dmu0 = mu(z0, t2) - mu(z0, t0);
    const double dmu1 = mu(z1, t2) - mu(z1, t0);
    const double dmu2 = mu(z2, t2) - mu(z2, t0);

    return (mu(z1, t2) + mu(z1, t0)) / 2
           - GridParams_.tau * (b2 * dmu2 - (b2 + b1) * dmu1 + b1 * dmu0) / (4 * GridParams_.h2 * GridParams_.h2);
}

double TSolver::Lambda1(const std::vector<double>& layer, const size_t i1, const size_t i1Local, const size_t i2,
    const size_t j) const
{
    const double y0 = i1 == 0 ? muSublayer(i1 - 1, i2, j) : layer[GetIdx(i1Local - 1, i2, GridParams_.N2 - 1)];
    const double y1 = layer[GetIdx(i1Local, i2, GridParams_.N2 - 1)];
    const double y2 = i1 == GridParams_.N1 - 2 ? muSublayer(i1 + 1, i2, j) : layer[GetIdx(i1Local + 1, i2, GridParams_.N2 - 1)];

    const double a1 = (lambda(i1, i2, y1) + lambda(i1 - 1, i2, y0)) / 2;
    const double a2 = (lambda(i1 + 1, i2, y2) + lambda(i1, i2, y1)) / 2;
    return (a2 * y2 - (a2 + a1) * y1 + a1 * y0) / (GridParams_.h1 * GridParams_.h1);
}

double TSolver::Lambda2(const std::vector<double>& layer, const size_t i1, const size_t i2, const size_t i2Local,
    const size_t j) const
{
    const double x = Idx2Value(i1, GridParams_.h1);

    const double y0 = i2 == 0 ? Boundaries_.mu3(x, j * GridParams_.tau) : layer[GetIdx(i2Local - 1, i1)];
    const double y1 = layer[GetIdx(i2Local, i1)];
    const double y2 = i2 == GridParams_.N2 - 2 ? Boundaries_.mu4(x, j * GridParams_.tau) : layer[GetIdx(i2Local + 1, i1)];

    const double b1 = (lambda(i1, i2, y1) + lambda(i1, i2 - 1, y0)) / 2;
    const double b2 = (lambda(i1, i2 + 1, y2) + lambda(i1, i2, y1)) / 2;
    return (b2 * y2 - (b2 + b1) * y1 + b1 * y0) / (GridParams_.h2 * GridParams_.h2);
}

void TSolver::InitZeroLayer(const size_t rank, const size_t worldSize, std::vector<double>& layer) const {
    const size_t hFrom = rank * ((GridParams_.N2 - 1) / worldSize) + std::min(rank, (GridParams_.N2 - 1) % worldSize) - (rank > 0);
    const size_t paddedStripeSize = (GridParams_.N2 - 1) / worldSize + (rank < (GridParams_.N2 - 1) % worldSize)
        + (rank > 0) + (rank < worldSize - 1);

    for (size_t i2Local = 0; i2Local < paddedStripeSize; ++i2Local) {
        for (size_t i1 = 0; i1 < GridParams_.N1 - 1; ++i1) {
            layer[GetIdx(i2Local, i1)] = Boundaries_.u0(Idx2Value(i1, GridParams_.h1), Idx2Value(hFrom + i2Local, GridParams_.h2));
        }
    }
}

void TSolver::Step1(const size_t j, const size_t rank, const size_t worldSize, const std::vector<double>& hLayer,
    std::vector<double>& hSublayer) const
{
    const auto N1 = GridParams_.N1;
    const auto N2 = GridParams_.N2;
    const auto h1 = GridParams_.h1;
    const auto h2 = GridParams_.h2;
    const auto tau = GridParams_.tau;

    const double t = (j + 0.5) * GridParams_.tau;

    const size_t hFrom = rank * ((N2 - 1) / worldSize) + std::min(rank, (N2 - 1) % worldSize) - (rank > 0);
    const size_t paddedStripeSize = (N2 - 1) / worldSize + (rank < (N2 - 1) % worldSize)
                                    + (rank > 0) + (rank < worldSize - 1);

    for (size_t i2Local = (rank > 0); i2Local < paddedStripeSize - (rank < worldSize - 1); ++i2Local) {
        const size_t i2 = i2Local + hFrom;
        const double z = Idx2Value(i2, h2);

        // Iteration no. 0: use y's from previous layer as initial approximation.
        std::vector<double> y;
        y.reserve(N1);
        copy(
            hLayer.begin() + GetIdx(i2Local, 0),
            hLayer.begin() + GetIdx(i2Local + 1, 0),
            back_inserter(y));

        y.push_back(Boundaries_.mu2(z, t));
        assert(y.size() == N1);

        // Iterations.
        double delta;
        do {
            std::vector<double> alpha(N1 - 2);
            std::vector<double> beta(N1 - 2);

            delta = 0;

            double aPrev = (lambda(-1, i2, muSublayer(-1, i2, j)) + lambda(0, i2, y[0])) / 2;

            for (size_t i1 = 0; i1 < N1 - 1; ++i1) {
                const double x = Idx2Value(i1, h1);

                // Calculate a, rho, c.
                const double aNext = (lambda(i1, i2, y[i1]) + lambda(i1 + 1, i2, y[i1 + 1])) / 2;

                const double rhoValue = rho(i1, i2, y[i1]);

                // Calculate A, B, C, F.
                const double A = aPrev / (h1 * h1);
                const double B = aNext / (h1 * h1);
                const double C = (aPrev + aNext) / (h1 * h1)
                    + 2 * rhoValue * c(i1, i2) / tau;
                aPrev = aNext;

                double additive = 0;
                if (i1 == 0) {
                    additive = A * muSublayer(i1 - 1, i2, j);
                } else if (i1 == N1 - 2) {
                    additive = B * muSublayer(i1 + 1, i2, j);
                }
                const double F =
                    hLayer[GetIdx(i2Local, i1)] * 2 * rhoValue * c(i1, i2) / tau +
                    Lambda2(hLayer, i1, i2, i2Local, j) +
                    Material_->GetProperties(x, z).Q(x, z, t) + additive;

                if (i1 != N1 - 2) {
                    // Progonka forward sweep: calculate alpha, beta.
                    if (i1 == 0) {
                        alpha[i1] = B / C;
                        beta[i1] = F / C;
                    } else {
                        alpha[i1] = B / (C - A * alpha[i1 - 1]);
                        beta[i1] = (F + A * beta[i1 - 1]) / (C - A * alpha[i1 - 1]);
                    }
                } else {
                    // On the last step neither alpha nor beta is calculated.
                    // Find solution in point (N1 - 2) instead.
                    const double yNew = (F + A * beta[i1 - 1]) / (C - A * alpha[i1 - 1]);
                    delta = std::abs(yNew - y[i1]) / y[i1];
                    y[i1] = yNew;
                }
            }

            // Progonka backward sweep: update y's inplace. Count diff meanwhile.
            for (size_t i1 = N1 - 2; i1 > 0; --i1) {
                const double yNew = y[i1] * alpha[i1 - 1] + beta[i1 - 1];
                delta = std::max(delta, std::abs(yNew - y[i1 - 1]) / y[i1 - 1]);
                y[i1 - 1] = yNew;
            }
        } while (delta > EPS);

        for (size_t i1 = 0; i1 < N1 - 1; ++i1) {
            hSublayer[GetIdx(i2Local, i1)] = y[i1];
        }
    }
}

void TSolver::Step2(const size_t j, const size_t rank, const size_t worldSize, const std::vector<double>& vSublayer,
    std::vector<double>& vLayer) const
{
    const auto N1 = GridParams_.N1;
    const auto N2 = GridParams_.N2;
    const auto h1 = GridParams_.h1;
    const auto h2 = GridParams_.h2;
    const auto tau = GridParams_.tau;

    const double t = (j + 1) * tau;

    const size_t vFrom = rank * ((N1 - 1) / worldSize) + std::min(rank, (N1 - 1) % worldSize) - (rank > 0);
    const size_t paddedStripeSize = (N1 - 1) / worldSize + (rank < (N1 - 1) % worldSize)
                                    + (rank > 0) + (rank < worldSize - 1);

    for (size_t i1Local = (rank > 0); i1Local < paddedStripeSize - (rank < worldSize - 1); ++i1Local) {
        const size_t i1 = i1Local + vFrom;
        const double x = Idx2Value(i1, h1);

        // Iteration no. 0: use y's from previous sublayer as initial approximation.
        std::vector<double> y;
        y.reserve(N2);
        copy(
            vSublayer.begin() + GetIdx(i1Local, 0, N2 - 1),
            vSublayer.begin() + GetIdx(i1Local + 1, 0, N2 - 1),
            back_inserter(y));

        y.push_back(Boundaries_.mu4(x, t));
        assert(y.size() == N2);

        // Iterations.
        double delta;
        do {
            std::vector<double> alpha(N2 - 2);
            std::vector<double> beta(N2 - 2);

            delta = 0;

            double bPrev = (lambda(i1, -1, Boundaries_.mu3(x, t)) + lambda(i1, 0, y[0])) / 2;

            for (size_t i2 = 0; i2 < N2 - 1; ++i2) {
                const double z = Idx2Value(i2, h2);

                // Calculate b, rho, c.
                const double bNext = (lambda(i1, i2, y[i2]) + lambda(i1, i2 + 1, y[i2 + 1])) / 2;

                const double rhoValue = rho(i1, i2, y[i2]);

                // Calculate A, B, C, F.
                const double A = bPrev / (h2 * h2);
                const double B = bNext / (h2 * h2);
                const double C = (bPrev + bNext) / (h2 * h2)
                                 + 2 * rhoValue * c(i1, i2) / tau;
                bPrev = bNext;

                double additive = 0;
                if (i2 == 0) {
                    additive = A * Boundaries_.mu3(x, t);
                } else if (i2 == N2 - 2) {
                    additive = B * Boundaries_.mu4(x, t);
                }
                const double F =
                    vSublayer[GetIdx(i1Local, i2, N2 - 1)] * 2 * rhoValue * c(i1, i2) / tau +
                    Lambda1(vSublayer, i1, i1Local, i2, j) +
                    Material_->GetProperties(x, z).Q(x, z, t) + additive;

                if (i2 != N2 - 2) {
                    // Progonka forward sweep: calculate alpha, beta.
                    if (i2 == 0) {
                        alpha[i2] = B / C;
                        beta[i2] = F / C;
                    } else {
                        alpha[i2] = B / (C - A * alpha[i2 - 1]);
                        beta[i2] = (F + A * beta[i2 - 1]) / (C - A * alpha[i2 - 1]);
                    }
                } else {
                    // On the last step neither alpha nor beta is calculated.
                    // Find solution in point (N2 - 2) instead.
                    const double yNew = (F + A * beta[i2 - 1]) / (C - A * alpha[i2 - 1]);
                    delta = std::abs(yNew - y[i2]) / y[i2];
                    y[i2] = yNew;
                }
            }

            // Progonka backward sweep: update y's inplace. Count diff meanwhile.
            for (size_t i2 = N2 - 2; i2 > 0; --i2) {
                const double yNew = y[i2] * alpha[i2 - 1] + beta[i2 - 1];
                delta = std::max(delta, std::abs(yNew - y[i2 - 1]) / y[i2 - 1]);
                y[i2 - 1] = yNew;
            }
        } while (delta > EPS);

        for (size_t i2 = 0; i2 < N2 - 1; ++i2) {
            vLayer[GetIdx(i1Local, i2, N2 - 1)] = y[i2];
        }
    }
}

void TSolver::DumpLayer(const void* const hLayerPtr, const size_t j, const std::vector<int>& stripes2,
    const MPI::Datatype& row, std::ostream& out) const
{
    MPI::Comm& world = MPI::COMM_WORLD;
    const size_t worldSize = world.Get_size();
    const size_t rank = world.Get_rank();

    std::vector<int> fullDispls(worldSize);
    for (size_t i = 1; i < worldSize; ++i) {
        fullDispls[i] = fullDispls[i - 1] + stripes2[i - 1];
    }

    if (DumpParams_.Frequency > 0 && j % DumpParams_.Frequency == 0) {
        std::vector<double> fullLayer;
        if (rank == 0) {
            fullLayer.resize((GridParams_.N1 - 1) * (GridParams_.N2 - 1));
        }
        world.Gatherv(
            hLayerPtr, stripes2[rank], row,
            fullLayer.data(), stripes2.data(), fullDispls.data(), row, 0);

        if (rank == 0) {
            out << "j=" << j << '\n';
            for (size_t i2 = 0; i2 < GridParams_.N2 - 1; ++i2) {
                for (size_t i1 = 0; i1 < GridParams_.N1 - 1; ++i1) {
                    out << fullLayer[GetIdx(i2, i1)] << '\t';
                }
                out << '\n';
            }
        }
    }
}
