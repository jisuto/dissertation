#include "grid_material.h"

#include <sstream>

namespace {

size_t ValueToIdx(double value, double step) {
    return static_cast<size_t>(std::floor(value / step));
}

void CheckIndex(size_t i, size_t N) {
    if (i > N) {
        std::stringstream ss;
        ss << "Index "  << i << " out of bounds " << N;
        throw std::runtime_error(ss.str());
    }
}

}

TGridMaterial::TGridMaterial(TGrid grid)
    : Grid_(std::move(grid))
{
    if (Grid_.Cells.size() != Grid_.N1 * Grid_.N2) {
        throw std::runtime_error("Inconsistent grid dimensions and size.");
    }
}

const TProperties& TGridMaterial::GetProperties(double x, double z) const {
    if (x < 0 || z < 0) {
        throw std::runtime_error("Value out of bounds.");
    }
    const size_t i1 = ValueToIdx(x, Grid_.h1);
    const size_t i2 = ValueToIdx(z, Grid_.h2);

    CheckIndex(i1, Grid_.N1);
    CheckIndex(i2, Grid_.N2);

    return Grid_.Cells[i1 + Grid_.N1 * i2];
}
