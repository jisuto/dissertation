#pragma once

#include <cmath>
#include <functional>

using T1ArgFunc = std::function<double(double)>;
using T2ArgFunc = std::function<double(double, double)>;
using T3ArgFunc = std::function<double(double, double, double)>;

struct TProperties {
    double c;
    T1ArgFunc rho;
    T1ArgFunc lambda;
    T3ArgFunc Q;
};

struct TBoundaries {
    /// x = 0
    T2ArgFunc mu1;
    /// x = N1
    T2ArgFunc mu2;
    /// z = 0
    T2ArgFunc mu3;
    /// z = N2
    T2ArgFunc mu4;

    T2ArgFunc u0;
};
