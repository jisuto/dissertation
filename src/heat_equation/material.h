#pragma once

#include "equation_params.h"

class IMaterial {
public:
    virtual ~IMaterial() = default;
    virtual const TProperties& GetProperties(double x, double z) const = 0;
};

class TUniformMaterial : public IMaterial {
public:
    explicit TUniformMaterial(TProperties params);
    const TProperties& GetProperties(double x, double z) const final;

private:
    const TProperties Properties_;
};
