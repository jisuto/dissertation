#pragma once

#include <cstddef>

struct TGridParams {
    size_t N1;
    size_t N2;
    size_t T;
    double h1;
    double h2;
    double tau;
};
