#pragma once

#include "dump_params.h"
#include "equation_params.h"
#include "grid_params.h"
#include "material.h"

#include <memory>
#include <vector>

namespace MPI {
    class Datatype;
}

class TSolver {
public:
    TSolver(std::unique_ptr<IMaterial> material, TBoundaries boundaries, TGridParams gridParams, TDumpParams dumpParams = {});

    void Run() const;

private:
    size_t GetIdx(size_t i, size_t j) const;
    size_t GetIdx(size_t i, size_t j, size_t offset) const;
    double Idx2Value(size_t i, double step) const;

    // Shortcuts.
    double lambda(size_t i1, size_t i2, double T) const;
    double rho(size_t i1, size_t i2, double T) const;
    double c(size_t i1, size_t i2) const;

    // On sublayer edges i1 = 0 and i1 = N1.
    double muSublayer(size_t i1, size_t i2, size_t j) const;

    double Lambda1(const std::vector<double>& layer, size_t i1, size_t i1Local, size_t i2, size_t j) const;
    double Lambda2(const std::vector<double>& layer, size_t i1, size_t i2, size_t i2Local, size_t j) const;

    void InitZeroLayer(size_t rank, size_t worldSize, std::vector<double>& layer) const;
    void Step1(size_t j, size_t rank, size_t worldSize, const std::vector<double>& hLayer,
        std::vector<double>& hSublayer) const;
    void Step2(size_t j, size_t rank, size_t worldSize, const std::vector<double>& vSublayer,
        std::vector<double>& vLayer) const;
    void DumpLayer(const void* hLayerPtr, size_t j, const std::vector<int>& stripes2,
        const MPI::Datatype& row, std::ostream& out) const;

private:
    const std::unique_ptr<IMaterial> Material_;
    const TBoundaries Boundaries_;
    const TGridParams GridParams_;
    const TDumpParams DumpParams_;
};
