#include <equation_params.h>
#include <grid_params.h>
#include <solver.h>

#include <functional>
#include <mpi.h>

using namespace std;

static constexpr size_t N1 = 100;
static constexpr size_t N2 = 100;
static constexpr size_t T = 100;
static constexpr double h1 = 0.01;
static constexpr double h2 = 0.01;
static constexpr double tau = 0.01;

TProperties GetExampleProperties() {
    return {
        .c = 1,

        .rho = [](double /*T*/) {
            return 1;
        },

        .lambda = [](double /*T*/) {
            return 1;
        },

        .Q = [](double x, double z, double t) {
            return -std::exp(x + z + t);
        },
    };
}

TBoundaries GetExampleBoundaries() {
    return {
        /// x = 0
        .mu1 = [](const double z, const double t) {
            return std::exp(z + t);
        },

        /// x = N1
        .mu2 = [](const double z, const double t) {
            return std::exp(z + t + h1 * N1);
        },

        /// z = 0
        .mu3 = [](const double x, const double t) {
            return std::exp(x + t);
        },

        /// z = N2
        .mu4 = [](const double x, const double t) {
            return std::exp(x + t + h2 * N2);
        },

        .u0 = [](const double x, const double z) {
            return std::exp(x + z);
        }
    };
}

TGridParams GetExampleGridParams() {
    return {
        .N1 = N1,
        .N2 = N2,
        .T = T,
        .h1 = h1,
        .h2 = h2,
        .tau = tau
    };
}

TDumpParams GetExampleDumpParams() {
    return {
        .Frequency = 49,
        .Filename = "example.txt"
    };
}

void RunExample() {
    TSolver solver(std::make_unique<TUniformMaterial>(GetExampleProperties()), GetExampleBoundaries(),
        GetExampleGridParams(), GetExampleDumpParams());
    solver.Run();
}

int main(int argc, char *argv[]) {
    MPI::Init(argc, argv);
    RunExample();
    MPI::Finalize();
    return 0;
}
