cmake_minimum_required(VERSION 3.10)
project(analytical CXX)

find_package(MPI)

include_directories(${HEAT_EQUATION_INCLUDE_PATH})
add_executable(${PROJECT_NAME} main.cpp)
target_link_libraries(${PROJECT_NAME} ${MPI_LIBRARIES} heat_equation)
