#include <equation_params.h>
#include <grid_material.h>
#include <grid_params.h>
#include <solver.h>

#include <functional>
#include <mpi.h>

using namespace std;

enum class MaterialType {
    TitaniumCarbide,
    Nichrome
};

TProperties GetMaterialProperties(MaterialType type) {
    switch (type) {
        case MaterialType::TitaniumCarbide:
            return {
                .c = 842,

                .rho = [](double /*T*/) {
                    return 4930;
                },

                .lambda = [](double T) {
                    return 2.798 + 0.014 * T;
                },

                .Q = [](double /*x*/, double /*z*/, double /*t*/) {
                    return 0;
                }
            };
        case MaterialType::Nichrome:
            return {
                .c = 460,

                .rho = [](double /*T*/) {
                    return 8200;
                },

                .lambda = [](double T) {
                    return 7.69833 + 0.0150363 * T + 2.178e-6 * T * T;
                },

                .Q = [](double /*x*/, double /*z*/, double /*t*/) {
                    return 0;
                }
            };
    }
}

TGrid GetGrid() {
    const size_t N1 = 63;
    const size_t N2 = 16;
    std::vector<TProperties> cells;
    cells.reserve(N1 * N2);

    for (size_t i2 = 0; i2 < N2; ++i2) {
        for (size_t i1 = 0; i1 < N1; ++i1) {
            if (i1 % 3 == 0 || i2 % 2 == 0) {
                cells.emplace_back(GetMaterialProperties(MaterialType::Nichrome));
            } else {
                cells.emplace_back(GetMaterialProperties(MaterialType::TitaniumCarbide));
            }
        }
    }

    return {
        .N1 = N1,
        .N2 = N2,
        .h1 = 0.00016,
        .h2 = 0.00016,
        .Cells = cells
    };
}

TBoundaries GetBoundaries(const TGridParams& gridParams) {
    return {
        /// x = 0
        .mu1 = [](double z, double t) {
            return 300;
        },

        /// x = N1
        .mu2 = [](double z, double t) {
            return 300;
        },

        /// z = 0
        .mu3 = [](double x, double t) {
            return 300;
        },

        /// z = N2
        .mu4 = [N1 = gridParams.N1, h1 = gridParams.h1](double x, double t) {
            static constexpr double ROUND_TRIP_TIME = 0.4;
            const double r = std::fmod(t, ROUND_TRIP_TIME) / ROUND_TRIP_TIME;
            const double center = 2 * N1 * h1 * (r < 0.5 ? r : (1 - r));
            x = (x - center) * 1000;
            const auto comp = std::exp(0.1997 * x + 0.000137813 * std::pow(x, 3));
            return std::max((38.0362 * comp * (483.023 + x * x)) / std::pow((1 + comp), 2), 300.);
        },

        .u0 = [](const double x, const double z) {
            return 300;
        }
    };
}

TGridParams GetGridParams() {
    return {
        .N1 = 500,
        .N2 = 120,
        .T = 10000,
        .h1 = 0.00002,
        .h2 = 0.00002,
        .tau = 0.0001
    };
}

TDumpParams GetDumpParams() {
    return {
        .Frequency = 50,
        .Filename = "plasma_jet_moving.txt"
    };
}

void RunExample() {
    TGridParams gridParams = GetGridParams();
    TSolver solver(std::make_unique<TGridMaterial>(GetGrid()), GetBoundaries(gridParams), gridParams, GetDumpParams());
    solver.Run();
}

int main(int argc, char *argv[]) {
    MPI::Init(argc, argv);
    RunExample();
    MPI::Finalize();
    return 0;
}
